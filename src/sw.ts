import { precacheAndRoute } from 'workbox-precaching';

declare let self: ServiceWorkerGlobalScope

const APP_SERVER_KEY = 'BN3W5M7wupjtxtxTUTm96duhXouxBqwUoByX5ShfwgyOYXSybqsBtaFYCYkAd9BeVpkYbR-zFpCPnU-Co0t0B1I';
//"BM-ud-nM678RHOwGNTwWVmgMoNZQHEf5Ujg9-kiaUxQkDnPyG8NM886bgXppa8CFtbZoTiZwXkz1yLx3xxdhfoE"
// vAX5mQVUYBDsYAzBCbQmRbzMqjzvYbswx5OLrlZF0pYc

function urlB64ToUint8Array(base64String: string | any[]) {
  const padding = '='.repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

const msg2json = (msg: string) => { try { return JSON.parse(msg) } catch (e) { return msg } }

self.addEventListener('install', (event) => {
  self.skipWaiting();
});

self.addEventListener('push', function (event: any) {
  console.log('[Service Worker] Push Received.');
  console.log(`[Service Worker] Push had this data: "${event.data.text()}"`);

  const title = 'Push Codelab';
  const options = {
    body: 'Yay it works.',
    icon: 'images/icon.png',
    badge: 'images/badge.png'
  };

  event.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', function (event) {
  console.log('[Service Worker] Notification click Received.');

  event.notification.close();

  event.waitUntil(
    clients.openWindow('https://developers.google.com/web/')
  );
});

self.addEventListener('pushsubscriptionchange', function (event) {
  console.log('[Service Worker]: \'pushsubscriptionchange\' event fired.');
  const applicationServerKey = urlB64ToUint8Array(APP_SERVER_KEY);
  event.waitUntil(
    self.registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: applicationServerKey
    })
      .then(function (newSubscription) {
        // TODO: Send to application server
        console.log('[Service Worker] New subscription: ', newSubscription);
      })
  );
});

self.addEventListener('message', event => {
  console.log({ event })
  if (event.data && event.data.type === 'SKIP_WAITING')
    self.skipWaiting()
})

// self.__WB_MANIFEST is default injection point
precacheAndRoute(self.__WB_MANIFEST)
